<?php

return [
    'modelMap' => [
        'Banner' => [
            'alias' => 'yiicod\bannersection\models\BannerModel',
            'class' => 'yiicod\bannersection\models\BannerModel',
            'fieldTitle' => 'title',
            'fieldContent' => 'content',
            'fieldPosition' => 'position',
            'fieldType' => 'type',
            //enumerables or array with params
            'position' => [
            ],
            'countPerPosition' => [
            ],
            'typePerPosition' => [
            ],            
        ],
        'BannerImage' => [
            'alias' => 'yiicod\bannersection\models\BannerImageModel',
            'class' => 'yiicod\bannersection\models\BannerImageModel',
            'fieldName' => 'name',
            'fieldBannerId' => 'bannerId',
            'fieldAlt' => 'alt',
            'fieldLink' => 'link',
            'fieldText' => 'text',
        ]
    ],
    'controllers' => [
        'controllerMap' => [
            'admin' => [
                'banner' => 'yiicod\bannersection\controllers\admin\BannerController',
                'bannerImage' => 'yiicod\bannersection\controllers\admin\BannerImageController',
            ]
        ],
        'admin' => [
            'banner' => [
                'layout' => '',
                'filters' => [],
                'accessRules' => [],
            ],
            'bannerImage' => [
                'layout' => '',
                'filters' => [],
                'accessRules' => [],
            ],
        ],
    ],
    'components' => [
    ],
];

