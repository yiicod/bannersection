<?php

/**
 * Base action
 * @author Orlov Alexey <aaorlov88@gmail.com>
 */

namespace yiicod\bannersection\actions;

use Yii;
use CAction;
use CHttpException;
use CActiveForm;

class BaseAction extends CAction
{

    /**
     * Performs the AJAX validation.
     * @author Orlov Alexey <aaorlov88@gmail.com>
     * @param BannerModel $model the model to be validated
     */
    protected function performAjaxValidation($model, $ajax = null)
    {
        if (Yii::app()->request->isAjaxRequest) {
            if (null === $ajax || Yii::app()->request->getParam('ajax', '') == $ajax) {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return BannerModel the loaded model
     * @throws CHttpException
     */
    public function loadModel($id, $class)
    {
        $model = $class::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

}
