<?php

namespace yiicod\bannersection\actions\admin\banner;

use CHtml;
use Yii;
use yiicod\bannersection\actions\BaseAction;

class CreateAction extends BaseAction
{

    public $view = 'yiicod.bannersection.views.admin.banner.create';

    /**
     * Creates a new model.
     * @author Orlov Alexey <Orlov.Alexey@zfort.net>
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function run()
    {
        $bannerModel = Yii::app()->getComponent('bannersection')->modelMap['Banner']['class'];
        $model = new $bannerModel;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model, 'banner-model-form');

        if (isset($_POST[CHtml::modelName($model)])) {
            $model->attributes = $_POST[CHtml::modelName($model)];
            if ($model->save()) {
                Yii::app()->user->setFlash('success',  Yii::t('bannersection','Record create success'));
                Yii::app()->controller->redirect(['update', 'id' => $model->id]);
            }
        }

        Yii::app()->controller->render($this->view, [
            'model' => $model,
        ]);
    }

}
