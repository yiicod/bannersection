<?php

namespace yiicod\bannersection\actions\admin\banner;

use yiicod\bannersection\actions\BaseAction;
use Yii;

class DeleteAction extends BaseAction
{

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function run($id)
    {
        $this->loadModel($id, Yii::app()->getComponent('bannersection')->modelMap['Banner']['class'])
                ->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['admin']);
        }
    }

}
