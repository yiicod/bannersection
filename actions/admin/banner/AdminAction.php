<?php

namespace yiicod\bannersection\actions\admin\banner;

use CHtml;
use Yii;
use yiicod\bannersection\actions\BaseAction;

class AdminAction extends BaseAction
{

    public $view = 'yiicod.bannersection.views.admin.banner.admin';

    /**
     * Manages all models.
     */
    public function run()
    {
        $bannerModel = Yii::app()->getComponent('bannersection')->modelMap['Banner']['class'];
        $model = new $bannerModel('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET[CHtml::modelName($bannerModel)]))
            $model->attributes = $_GET[CHtml::modelName($bannerModel)];

       Yii::app()->controller->render($this->view, [
            'model' => $model,
        ]);
    }

}

