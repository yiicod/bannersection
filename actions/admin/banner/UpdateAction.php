<?php

namespace yiicod\bannersection\actions\admin\banner;

use CHtml;
use Yii;
use yiicod\bannersection\actions\BaseAction;

class UpdateAction extends BaseAction
{

    public $view = 'yiicod.bannersection.views.admin.banner.update';

    /**
     * Updates a particular model.* 
     * If update is successful, the browser will be redirected to the 'view' page.
     * @author Orlov Alexey <Orlov.Alexey@zfort.net>
     * @param integer $id the ID of the model to be updated
     */
    public function run($id)
    {
        $model = $this->loadModel($id, Yii::app()->getComponent('bannersection')->modelMap['Banner']['class']);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model, 'banner-model-form');

        if (isset($_POST[CHtml::modelName($model)])) {
            $model->attributes = $_POST[CHtml::modelName($model)];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', Yii::t('bannersection','Record update success'));
                Yii::app()->controller->redirect(['update', 'id' => $model->id]);
            }
        }

        Yii::app()->controller->render($this->view, [
            'model' => $model,
        ]);
    }

}
