<?php

namespace yiicod\bannersection\actions\admin\bannerImage;

use CJSON;
use Yii;
use yiicod\bannersection\actions\BaseAction;

class UpdateAction extends BaseAction
{

    /**
     * Ajax update from grid view
     * @author Orlov Alexey <Orlov.Alexey@zfort.net>
     */
    public function run()
    {
        $model = $this->loadModel(Yii::app()->request->getParam('pk', 0), Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['class']);
        $model->{Yii::app()->request->getParam('name')} = Yii::app()->request->getParam('value');
        $model->save(false);
        
        echo CJSON::encode([]);
    }

}