<?php

namespace yiicod\bannersection\controllers\admin;

use Controller;
use Yii;
use CMap;

/**
 * BannerController class
 * @author Orlov Alexey <Orlov.Alexey@zfort.net>
 */
class BannerImageController extends Controller
{

    public function init()
    {
        parent::init();

        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;
        if (Yii::app()->getComponent('bannersection')->controllers[$module][Yii::app()->controller->id]['layout']) {
            $this->layout = Yii::app()->getComponent('bannersection')->controllers[$module][Yii::app()->controller->id]['layout'];
        }
    }

    public function filters()
    {
        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;
        return CMap::mergeArray(parent::filters(), CMap::mergeArray(
                                [
                            'accessControl',
                            'ajaxOnly + update',
                                ], Yii::app()->getComponent('bannersection')->controllers[$module][Yii::app()->controller->id]['filters'])
        );
    }

    public function accessRules()
    {
        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;
        return CMap::mergeArray(parent::accessRules(), Yii::app()->getComponent('bannersection')->controllers[$module][Yii::app()->controller->id]['accessRules']
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return [
            'update' => [
                'class' => 'yiicod\bannersection\actions\admin\bannerImage\UpdateAction',
            ],
            'delete' => [
                'class' => 'yiicod\bannersection\actions\admin\bannerImage\DeleteAction',
            ],
            'imageUpload' => [
                'class' => 'yiicod\impraviext\actions\ImageUpload',
                'uploadPath' => Yii::getPathOfAlias('webroot') . '/uploads',
                'uploadUrl' => Yii::app()->getBaseUrl(true) . '/uploads',
                'params' => ['id'],
                'uploadCreate' => true,
            ],            
        ];
    }

}
