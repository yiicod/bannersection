<?php

namespace yiicod\bannersection\controllers\admin;

use Controller;
use Yii;
use CMap;

/**
 * 
 */
class BannerController extends Controller
{

    public $defaultAction = 'admin';

    public function init()
    {
        parent::init();

        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;
        if (Yii::app()->getComponent('bannersection')->controllers[$module][Yii::app()->controller->id]['layout']) {
            $this->layout = Yii::app()->getComponent('bannersection')->controllers[$module][Yii::app()->controller->id]['layout'];
        }
    }

    public function filters()
    {
        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;
        return CMap::mergeArray(parent::filters(), CMap::mergeArray(
                                [
                            'accessControl',
                            'ajaxOnly + imageUpload, cocoCod',
                                ], Yii::app()->getComponent('bannersection')->controllers[$module][Yii::app()->controller->id]['filters'])
        );
    }

    public function accessRules()
    {
        $module = Yii::app()->controller->module === null ? 'default' : Yii::app()->controller->module->id;
        return CMap::mergeArray(parent::accessRules(), Yii::app()->getComponent('bannersection')->controllers[$module][Yii::app()->controller->id]['accessRules']
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return [
            'admin' => [
                'class' => 'yiicod\bannersection\actions\admin\banner\AdminAction',
            ],
            'update' => [
                'class' => 'yiicod\bannersection\actions\admin\banner\UpdateAction',
            ],
            'create' => [
                'class' => 'yiicod\bannersection\actions\admin\banner\CreateAction',
            ],
            'delete' => [
                'class' => 'yiicod\bannersection\actions\admin\banner\DeleteAction',
            ],
            'cocoCod' => [
                'class' => 'yiicod\cococod\actions\FileUploadAction',
            ],
        ];
    }

}
