<?php

namespace yiicod\bannersection\widgets;

use Yii;
use CActiveDataProvider;
use CWidget;
use CJSON;
use CMap;
use CClientScript;
use CHtml;

class BannerWidget extends CWidget
{

    /**
     * Widget auto generate id
     */
    private $_id = null;

    /**
     * Data provider
     */
    public $dataProvider = null;

    /**
     * Flag, render or not title
     */
    public $renderTitle = true;

    /**
     * Banner position 
     */
    public $position = null;

    /**
     * html options
     */
    public $htmlOptions = [];

    /**
     * bx slider
     */
    public $bxSliderOptions = [];

    /**
     * image html options
     */
    public $imageHtmlOptions = [];

    /**
     * View item
     */
    public $itemView = 'yiicod.bannersection.widgets.views._bannerItem';

    /**
     * Run widget
     */
    public function run()
    {
        parent::run();

        $this->_id = uniqid('id');

        $this->registerPackage();

        Yii::app()->getClientScript()->registerScript(__CLASS__ . $this->_id, '$("#' . $this->_id . ' .bxslider").bxSlider(' .
                CJSON::encode(CMap::mergeArray(['infiniteLoop' => true, 'hideControlOnEnd' => true, 'auto' => true,], $this->bxSliderOptions))
                . ');', CClientScript::POS_READY
        );
//        echo CHtml::openTag('div', $this->htmlOptions);
        $this->widget('zii.widgets.CListView', [
            'id' => $this->_id,
            'dataProvider' => $this->getDataProvider(),
            'itemView' => $this->itemView,
            'viewData' => ['bannerWidget' => $this],
            'enablePagination' => false,
            'emptyText' => '',
            'htmlOptions' => $this->htmlOptions,
            'template' => '{items}'
        ]);
//        echo CHtml::closeTag('div');
        //$this->render('banner', array())
    }

    public function getDataProvider()
    {
        if ($this->dataProvider instanceof CActiveDataProvider) {
            return $this->dataProvider;
        } elseif (null !== $this->position) {
            $bannerClass = Yii::app()->getComponent('bannersection')->modelMap['Banner']['class'];
            return $bannerClass::model()->getDataProvider($this->position);
        }
    }

    public function registerPackage()
    {
//        Yii::setPathOfAlias('yiicod.bannersection.widgets.assets', rtrim(dirname(__FILE__), '/') . '/assets');
        Yii::app()->clientScript->addPackage('bx-slider', [
            'baseUrl' => Yii::app()->assetManager->publish(Yii::getPathOfAlias('yiicod.bannersection.widgets.assets')),
            'js' => ['jquery.bxslider/jquery.bxslider.js'],
            'css' => ['jquery.bxslider/jquery.bxslider.css'],
            'depends' => ['jquery'],
                ]
        );
        Yii::app()->clientScript->registerPackage('bx-slider');
    }

}
