<div class="banner banner-<?php echo $data->id ?>">    
    <?php if ($bannerWidget->renderTitle): ?>
        <h3 class="title"><?php echo $data->title ?></h3>
    <?php endif; ?>
    <?php if ($data->type == \yiicod\bannersection\models\enumerables\BannerType::TYPE_SLIDER): ?>
        <?php if ($data->images): ?>
            <div class="bxslider">
                <?php foreach ($data->images as $image): ?>
                    <div>
                        <?php if ($image->link): ?>
                            <a href="<?php echo $image->link ?>">
                                <?php echo CHtml::image($image->getFileSrc("name"), $image->alt, $this->imageHtmlOptions) ?>
                                <span class="text-slider"><?php echo $image->text ?></span>
                            </a>
                        <?php else: ?>    
                            <?php echo CHtml::image($image->getFileSrc("name"), $image->alt, $this->imageHtmlOptions) ?>
                            <span class="text-slider"><?php echo $image->text ?></span>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>            
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <?php if ($data->type == \yiicod\bannersection\models\enumerables\BannerType::TYPE_TEXT): ?>        
        <div>
            <?php echo $data->content ?>
        </div>
    <?php endif; ?>
</div>