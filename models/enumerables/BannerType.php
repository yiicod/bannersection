<?php

namespace yiicod\bannersection\models\enumerables;

class BannerType extends \CEnumerable
{
    /**
     * Banner type text
     * @author Orlov Alexey <Orlov.Alexey@zfort.net>
     * return int
     */

    const TYPE_TEXT = 1;
    /**
     * Banner type slider
     * @author Orlov Alexey <Orlov.Alexey@zfort.net>
     * return int
     */
    const TYPE_SLIDER = 2;

    /**
     * @return array
     */
    public static function listData()
    {
        return [
            self::TYPE_TEXT => 'Text',
            self::TYPE_SLIDER => 'Slider'
        ];
    }

    public static function getLabel($index)
    {
        $list = self::listData();
        if (isset($list[$index])) {
            return $list[$index];
        }
    }

}