<?php

namespace yiicod\bannersection\models;

use CDbCriteria;
use CActiveDataProvider;
use CActiveRecord;
use Yii;

/**
 * This is the model class for table "BannerModel".
 * @author Orlov Alexey <Orlov.Alexey@zfort.net>
 * The followings are the available columns in table 'BannerModel':
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $type
 */
class BannerModel extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'Banner';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['title, type, position', 'required'],
            ['title, type', 'length', 'max' => 100],
            ['content', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, title, content, type', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'images' => [self::HAS_MANY, 'yiicod\bannersection\models\BannerImageModel',
                [Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldBannerId'] => 'id']
            ],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('bannersection', 'ID'),
            'title' => Yii::t('bannersection', 'Title'),
            'content' => Yii::t('bannersection', 'Content'),
            'type' => Yii::t('bannersection', 'Type'),
            'position' => Yii::t('bannersection', 'Position')
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('position', $this->position, true);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Banner the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        $behaviors = [];
        if (file_exists(Yii::getPathOfAlias('application.models.behaviors.XssBehavior') . '.php')) {            
            $behaviors['XssBehavior'] = [
                'class' => 'application.models.behaviors.XssBehavior',                
            ];
        }

        $behaviors = \CMap::mergeArray($behaviors, [
                    'BannerBehavior' => [
                        'class' => 'yiicod\bannersection\models\behaviors\BannerBehavior'
                    ],
                    'BannerExtBehavior' => [
                        'class' => 'yiicod\bannersection\models\behaviors\BannerExtBehavior'
                    ]
                        ]
        );

        return \CMap::mergeArray(parent::behaviors(), $behaviors);
    }

}
