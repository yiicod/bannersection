<?php

namespace yiicod\bannersection\models\behaviors;

use Yii;
use CActiveRecordBehavior;

class BannerBehavior extends CActiveRecordBehavior
{

    /**
     * Set title
     * @param string $value
     */
    public function setTitle($value)
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldTitle'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldTitle']} = $value;
        }
    }

    /**
     * Set content
     * @param string $value
     */
    public function setContent($value)
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldContent'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldContent']} = $value;
        }
    }

    /**
     * Set isDefault
     * @param int $value
     */
    public function setPosition($value)
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldIsDefault'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldIsDefault']} = $value;
        }
    }

    /**
     * Set status
     * @param int $value
     */
    public function setType($value)
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldStatus'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldStatus']} = $value;
        }
    }

    /**
     * Get title
     * @return string
     */
    public function getTitle()
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldTitle'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldTitle']};
        }
        return '';
    }

    /**
     * Get Content
     * @return string
     */
    public function getContent()
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldContent'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldContent']};
        }
        return '';
    }

    /**
     * Get is default
     * @return string
     */
    public function getPosition()
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldPosition'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldPosition']};
        }
        return 0;
    }

    /**
     * Get status
     * @return int
     */
    public function getType()
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldType'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldType']};
        }
        return 0;
    }

}