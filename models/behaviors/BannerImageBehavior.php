<?php

namespace yiicod\bannersection\models\behaviors;

use Yii;
use CActiveRecordBehavior;

class BannerImageBehavior extends CActiveRecordBehavior
{

    /**
     * Set title
     * @param string $value
     */
    public function setName($value)
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldName'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldName']} = $value;
        }
    }

    /**
     * Set status
     * @param int $value
     */
    public function setBannerId($value)
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldBannerId'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldBannerId']} = $value;
        }
    }

    /**
     * Set content
     * @param string $value
     */
    public function setAlt($value)
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldAlt'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldAlt']} = $value;
        }
    }

    /**
     * Set isDefault
     * @param int $value
     */
    public function setLink($value)
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldLink'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldLink']} = $value;
        }
    }

    /**
     * Set status
     * @param int $value
     */
    public function setText($value)
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldText'], $this->getOwner()->attributeNames())) {
            $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldText']} = $value;
        }
    }

    /**
     * Get title
     * @return string
     */
    public function getName()
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldName'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldName']};
        }
        return '';
    }

    /**
     * Get title
     * @return string
     */
    public function getBannerId()
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldBannerId'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldBannerId']};
        }
        return 0;
    }

    /**
     * Get Content
     * @return string
     */
    public function getAlt()
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldAlt'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldAlt']};
        }
        return '';
    }

    /**
     * Get is default
     * @return string
     */
    public function getLink()
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldPosition'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldPosition']};
        }
        return 0;
    }

    /**
     * Get status
     * @return int
     */
    public function getText()
    {
        if (in_array(Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldText'], $this->getOwner()->attributeNames())) {
            return $this->getOwner()->{Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldText']};
        }
        return 0;
    }

}