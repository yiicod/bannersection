<?php

namespace yiicod\bannersection\models\behaviors;

use Yii;
use CActiveDataProvider;
use CActiveRecordBehavior;

class BannerImageExtBehavior extends CActiveRecordBehavior
{

    /**
     * Event for coco uploader
     */
    public function onFileUploaded($fullFileName, $userdata, $results)
    {        
        //Save to session
        $this->getOwner()->onAfterFileUploaded($fullFileName, Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldName']);
        $this->getOwner()->setBannerId($userdata[Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldBannerId']]);
        $this->getOwner()->setName($results['name']);
        $this->getOwner()->save(false);        
    }

    public function getDataProvider($id)
    {
        return new CActiveDataProvider(Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['class'], [
            'criteria' => [
                'condition' => Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldBannerId'] .
                '=:bannerId',
                'params' => [':bannerId' => $id],
            ]
                ]
        );
    }

}
