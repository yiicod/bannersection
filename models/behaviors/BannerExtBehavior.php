<?php

namespace yiicod\bannersection\models\behaviors;

use CActiveDataProvider;
use Yii;
use CActiveRecordBehavior;

class BannerExtBehavior extends CActiveRecordBehavior
{

    private $originalAttributes = [];

    public function getDataProvider($position)
    {
        $bannerClass = Yii::app()->getComponent('bannersection')->modelMap['Banner']['class'];
        return new CActiveDataProvider($bannerClass::model()->cache(Yii::app()->getComponent('bannersection')->cacheDuration, $this->getCacheDependency($position), 2), [
            'criteria' => [
                'condition' => 'position=:position',
                'params' => [':position' => $position],
            ]
                ]
        );
    }

    /**
     * Banner position return
     * @author Orlov Alexey <Orlov.Alexey@zfort.net>
     * return Array
     */
    public function getPositionListData($index = null)
    {
        $items = @call_user_func(Yii::app()->getComponent('bannersection')->modelMap['Banner']['position']);
        if (null === $items) {
            $items = Yii::app()->getComponent('bannersection')->modelMap['Banner']['position'];
        }
        if (!is_null($index)) {
            return isset($items[$index]) ? $items[$index] : $index;
        }
        return $items;
    }

    /**
     * Banner position return
     * @author Orlov Alexey <Orlov.Alexey@zfort.net>
     * return Array
     */
    public function getCountPerPositionListData($index = null)
    {
        $items = @call_user_func(Yii::app()->getComponent('bannersection')->modelMap['Banner']['countPerPosition']);
        if (null === $items) {
            $items = Yii::app()->getComponent('bannersection')->modelMap['Banner']['countPerPosition'];
        }
        if (!is_null($index) && isset($items[$index])) {
            return $items[$index];
        }
        return $items;
    }

    /**
     * Banner position return
     * @author Orlov Alexey <Orlov.Alexey@zfort.net>
     * return Array
     */
    public function getTypePerPositionListData($index = null)
    {
        $items = @call_user_func(Yii::app()->getComponent('bannersection')->modelMap['Banner']['typePerPosition']);
        if (null === $items) {
            $items = Yii::app()->getComponent('bannersection')->modelMap['Banner']['typePerPosition'];
        }
        if (!is_null($index) && isset($items[$index])) {
            return $items[$index];
        }
        return $items;
    }

    /**
     * Remove images
     * @param CEvent $event
     */
    public function afterDelete($event)
    {
        parent::afterDelete($event);

        Yii::app()->setGlobalState($this->getStateName($this->owner->{Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldPosition']}), time());

        $bannerImage = Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['class'];
        $bannerImageModel = $bannerImage::model()->findAllByAttributes([
            Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldBannerId'] => $this->getOwner()->id
        ]);
        foreach ($bannerImageModel as $record) {
            $record->delete();
        }
    }

    /**
     * Validate by rules
     * @param CEvent $event
     */
    public function beforeValidate($event)
    {
        parent::beforeValidate($event);

        if ($this->getOwner()->isNewRecord ||
                $this->originalAttributes[Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldPosition']] != $this->getOwner()->getPosition()
        ) {
            if (in_array($this->getOwner()->getPosition(), array_keys($this->getCountPerPositionListData()))) {
                $bannerImage = Yii::app()->getComponent('bannersection')->modelMap['Banner']['class'];
                $count = $bannerImage::model()->count(
                        Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldPosition'] . '=:position', [
                    'position' => $this->getOwner()->getPosition()
                        ]
                );
                $maxCount = $this->getCountPerPositionListData($this->getOwner()->getPosition());
                if ($count >= $maxCount) {
                    $this->getOwner()->addError(Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldPosition'], 'Max banners can be: ' . $maxCount);
                }
            }
        }

        if (in_array($this->getOwner()->getPosition(), array_keys($this->getTypePerPositionListData()))) {
            if ($this->getOwner()->getType() != $this->getTypePerPositionListData($this->getOwner()->getPosition())) {
                $this->getOwner()->addError(Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldType'], 'You can not use this type');
            }
        }
    }

    /**
     * Get original attrs
     * @param CEvent $event
     */
    public function afterFind($event)
    {
        parent::afterFind($event);

        $this->originalAttributes = $this->getOwner()->getAttributes();
    }

    /**
     * Call before save
     * @param CEvent $event
     * @return boolean
     */
    public function beforeSave($event)
    {
        $this->getOwner()->setContent(stripslashes($this->getOwner()->getContent()));

        Yii::app()->setGlobalState($this->getStateName($this->owner->{Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldPosition']}), time());

        return parent::beforeSave($event);
    }

    /**
     * Get state name
     * @param int $posistion
     * @return string
     */
    private function getStateName($posistion)
    {
        return md5(__CLASS__ . $posistion);
    }

    /**
     * Genearete cache id
     * @param type $posistion
     */
    private function getCacheDependency($posistion)
    {
        $cache = new \CGlobalStateCacheDependency($this->getStateName($posistion));
        $cache->reuseDependentData = false;

        return $cache;
    }

}
