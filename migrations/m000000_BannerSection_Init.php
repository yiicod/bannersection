<?php

class m000000_Comments_Init extends CDbMigration
{

    public function up()
    {
        $this->execute("
            CREATE TABLE `Banner` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `title` varchar(45) NOT NULL,
              `content` text,
              `position` int(11) NOT NULL,
              `type` int(11) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
        $this->execute("
            CREATE TABLE `BannerImage` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `bannerId` int(11) NOT NULL,
              `name` varchar(100) NOT NULL,
              `alt` varchar(100) DEFAULT NULL,
              `link` varchar(100) DEFAULT NULL,
              `text` text,
              PRIMARY KEY (`id`),
              KEY `i_bannerId` (`bannerId`),
              CONSTRAINT `fk_BannerImage_bannerId` FOREIGN KEY (`bannerId`) REFERENCES `Banner` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
    }

    public function down()
    {
        $this->execute("
            DROP TABLE `BannerImage`;
        ");
        $this->execute("
            DROP TABLE `Banner`;
        ");
    }

}
