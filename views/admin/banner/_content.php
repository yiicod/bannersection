
<div class="control-group ">

    <div class="controls editor-row">
        <?php
        $this->widget('vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget', [
            'model' => $model,
            'attribute' => Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldContent'],
            'options' => [
                'imageUpload' => Yii::app()->createAbsoluteUrl('/admin/bannerImage/imageUpload', [
                    'attr' => Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldContent'],
                        ]
                ),
                'removeWithoutAttr' => false,
                'minHeight' => '200',
                'replaceDivs' => false,
            ],
        ]);
        ?>
    </div>
</div>