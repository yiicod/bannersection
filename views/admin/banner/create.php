<?php
/* @var $this BannerController */
/* @var $model BannerModel */

$this->breadcrumbs = [
    Yii::t('bannersection', 'Manage Banners') => ['admin'],
    Yii::t('bannersection', 'Create'),
];

$this->menu = [
    ['label' => Yii::t('bannersection', 'Manage Banners'), 'url' => ['admin']],    
];
?>

<h1>Create Banner</h1>

<?php $this->renderPartial('yiicod.bannersection.views.admin.banner._form', ['model' => $model]); ?>