<?php
/* @var $this BannerController */
/* @var $model BannerModel */
/* @var $form CActiveForm */
?>

<div class="form form-box">

    <?php
    $form = $this->beginWidget('CActiveForm', [
        'id' => 'banner-model-form',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'htmlOptions' => ['class' => 'well email-well form-horizontal'], // for inset effect
        'clientOptions' => [
            'validateOnChange' => false,
            'validateOnSubmit' => true,
        ],
    ]);
    ?>

    <p class="note"><?php echo Yii::t('bannersection', 'Fields with {tag} are required.', ['{tag}' => '<span class="required">*</span>']) ?></p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="control-group ">
            <?php echo $form->labelEx($model, Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldTitle'], ['class' => 'control-label']); ?>
            <div class="controls">
                <?php echo $form->textField($model, Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldTitle'], ['size' => 60, 'maxlength' => 100]); ?>
                <?php echo $form->error($model, Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldTitle']); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="control-group ">
            <?php echo $form->labelEx($model, Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldType'], ['class' => 'control-label']); ?>
            <div class="controls">
                <?php echo $form->dropDownList($model, Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldType'], yiicod\bannersection\models\enumerables\BannerType::listData()); ?>
                <?php echo $form->error($model, Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldType']); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="control-group ">
            <?php echo $form->labelEx($model, Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldPosition'], ['class' => 'control-label']); ?>
            <div class="controls">
                <?php echo $form->dropDownList($model, Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldPosition'], $model->getPositionListData()); ?>
                <?php echo $form->error($model, Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldPosition']); ?>
            </div>
        </div>
    </div>
    <div class="row">

    </div>

    <?php if (!$model->isNewRecord): ?>
        <?php if ($model->type == \yiicod\bannersection\models\enumerables\BannerType::TYPE_TEXT): ?>
            <div class="row">
                <?php $this->renderPartial('yiicod.bannersection.views.admin.banner._content', ['form' => $form, 'model' => $model]); ?>
            </div>
        <?php else: ?>
            <div class="row">
                <?php $this->renderPartial('yiicod.bannersection.views.admin.banner._images', ['model' => $model]); ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <div class="form-actions full-buttons clearfix">
        <?php
        echo CHtml::submitButton($model->isNewRecord ? Yii::t('bannersection', 'Create') : Yii::t('bannersection', 'Save'));
        ?>
        <?php
        echo CHtml::resetButton(Yii::t('bannersection', 'Cancel'), ['class' => 'cancel', 'onclick' => 'window.location="' . Yii::app()->createUrl('/admin/banner') . '"']);
        ?>        
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->