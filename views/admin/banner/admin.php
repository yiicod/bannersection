<?php
/* @var $this BannerController */
/* @var $model BannerModel */

$this->breadcrumbs = [
    Yii::t('bannersection', 'Manage Banners'),
];

$this->menu = [
    ['label' => Yii::t('bannersection', 'Create'), 'url' => ['create']],
];
?>

<h1><?php echo Yii::t('bannersection', 'Manage Banners') ?></h1>

<?php
$this->widget('zii.widgets.grid.CGridView', [
    'id' => 'banner-model-grid',
    'dataProvider' => $model->search(),
    'itemsCssClass' => 'items table table-striped table-bordered',
    'filter' => $model,
    'columns' => [
        Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldTitle'],
        [
            'name' => Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldType'],
            'value' => 'yiicod\bannersection\models\enumerables\BannerType::getLabel($data->{Yii::app()->getComponent(\'bannersection\')->modelMap[\'Banner\'][\'fieldType\']})',
            'filter' => yiicod\bannersection\models\enumerables\BannerType::listData(),
        ],
        [
            'name' => Yii::app()->getComponent('bannersection')->modelMap['Banner']['fieldPosition'],
            'value' => 'yiicod\bannersection\helpers\DataHelper::model(\'Banner\', false)->getPositionListData($data->{Yii::app()->getComponent(\'bannersection\')->modelMap[\'Banner\'][\'fieldPosition\']})',
            'filter' => yiicod\bannersection\helpers\DataHelper::model('Banner', false)->getPositionListData(),
        ],
        [
            'header' => Yii::t('bannersection', 'Actions'),
            'template' => '{update}{delete}',
        ],
    ],
]);
?>
