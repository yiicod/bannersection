
<div class="control-group ">

    <div class="upload-row">
        <?php
        $this->widget('yiicod\cococod\widgets\FileUploadWidget', [
            'id' => 'cocowidget0',
            'allowedExtensions' => ['jpeg', 'jpg', 'gif', 'png'],
            'maxFileSize' => 10 * 1024 * 1024, // limit in server-side and in client-side 10mb
            'uploadDir' => Yii::getPathOfAlias('webroot') . '/uploads/temp/', // coco will @mkdir it
            'uploadUrl' => Yii::app()->getBaseUrl(true) . '/uploads/temp/', // coco will @mkdir it
            'receptorClassName' => Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['class'],
            'methodName' => 'onFileUploaded',
            'userdata' => [Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldBannerId'] => $model->id],
            'maxUploads' => -1, // defaults to -1 (unlimited)   
            'multiple' => true, // true or false, defaults: true
            'buttonText' => 'Upload file',
            'dropFilesText' => 'Upload or Drop here',
            'events' => [
                'stop' => 'js:function(e){
                    $.fn.yiiGridView.update("image-list");
                    $("#cocowidget0 .progress").hide(0);                
                }',
            ],
            'htmlOptions' => ['style' => 'width: 300px;'],
            'defaultUrl' => ['/admin/banner/cocoCod'],
        ]);
        ?>
    </div>
</div>
<div class="row">
    <?php
    $bannerImageModel = Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['class'];

    $dataProvider = $bannerImageModel::model()->getDataProvider($model->id);

    $columns[] = [
        'name' => Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldName'],
        'type' => 'raw',
        'htmlOptions' => ['style' => 'width: 60px'],
        'value' => 'CHtml::image($data->getFileSrc("' . Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldName'] . '"), "", array("width" => "50"))',
    ];
    if (in_array(Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldAlt'], $bannerImageModel::model()->attributeNames())) {
        $columns[] = [
            'name' => Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldAlt'],
            'class' => 'vendor.vitalets.x-editable-yii.EditableColumn',
            'headerHtmlOptions' => ['style' => 'width:80px'],
            'editable' => [
                'type' => 'text',
                //'mode' => 'popup',
                'url' => Yii::app()->createAbsoluteUrl('/admin/bannerImage/update'),
            ]
        ];
    }
    if (in_array(Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldLink'], $bannerImageModel::model()->attributeNames())) {
        $columns[] = [
            'name' => Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldLink'],
            'class' => 'vendor.vitalets.x-editable-yii.EditableColumn',
            'headerHtmlOptions' => ['style' => 'width:80px'],
            'editable' => [
                'type' => 'text',
                //'mode' => 'popup',
                'url' => Yii::app()->createAbsoluteUrl('/admin/bannerImage/update'),
            ]
        ];
    }
    if (in_array(Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldText'], $bannerImageModel::model()->attributeNames())) {
        $columns[] = [
            'name' => Yii::app()->getComponent('bannersection')->modelMap['BannerImage']['fieldText'],
            'class' => 'vendor.vitalets.x-editable-yii.EditableColumn',
            'headerHtmlOptions' => ['style' => 'width:80px'],
            'editable' => [
                'type' => 'text',
                //'mode' => 'popup',
                'url' => Yii::app()->createAbsoluteUrl('/admin/bannerImage/update'),
            ]
        ];
    }

    $columns[] = [
        'htmlOptions' => ['nowrap' => 'nowrap'],
        'class' => 'CButtonColumn',
        'template' => '{delete}',
        'deleteButtonUrl' => 'Yii::app()->controller->createUrl("/admin/bannerImage/delete",array("id"=>$data->primaryKey))',
    ];

    Yii::app()->clientScript->registerCss('qq-upload-list-disable', "
            .qq-upload-list{
                display:none!important;
            }
        ");

    $this->widget('zii.widgets.grid.CGridView', [
        'id' => 'image-list',
        'dataProvider' => $dataProvider,
        'template' => "{items}",
        'columns' => $columns,
            ]
    );
    ?>
</div>