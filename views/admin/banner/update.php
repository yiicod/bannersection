<?php
/* @var $this BannerController */
/* @var $model BannerModel */
$this->breadcrumbs = [
    Yii::t('bannersection', 'Manage Banners') => ['admin'],
    Yii::t('bannersection', 'Update'),
];

$this->menu = [
    ['label' => Yii::t('bannersection', 'Manage Banners'), 'url' => ['admin']],
    ['label' => Yii::t('bannersection', 'Create'), 'url' => ['create']],
];
?>

<h1><?php echo Yii::t('bannersection', 'Update Banner')?> "<?php echo $model->title; ?>"</h1>

<?php $this->renderPartial('yiicod.bannersection.views.admin.banner._form', ['model' => $model]); ?>