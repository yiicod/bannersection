bannersection extensions
========================

Didn't tested with different url rules

Config ( This is all config for extensions ):
---------------------------------------------

Insert into your composer:
--------------------------
```php

"yiicod/bannersection":"version"
...
"repositories": [
    {
        "type": "git",
        "url": "https://bitbucket.org/codteam/yiicod-bannersection.git"    
    },
    {
        "type": "git",
        "url": "https://bitbucket.org/codteam/yiicod-impraviext.git"
    }        
]
...

```

Insert into config
------------------

```php
'aliases' => array(
    'vendor' => '...',
),
'components' => array(
        'bannersection' => array(
            'class' => 'yiicod\bannersection\BannerSection',
            'modelMap' => array(
                'Banner' => array(
                    'alias' => 'yiicod\bannersection\models\BannerModel',
                    'class' => 'yiicod\bannersection\models\BannerModel',
                    'fieldTitle' => 'title',
                    'fieldContent' => 'content',
                    'fieldPosition' => 'position',
                    'fieldType' => 'type',
                    'position' => array(
                        0 => 'Top Home Slider',
                        1 => 'Center Home Banners',
                    ),
                    'countPerPosition' => array(
                        0 => 1,
                        1 => 3,
                    ),
                ),
                'BannerImage' => array(
                    'alias' => 'yiicod\bannersection\models\BannerImageModel',
                    'class' => 'yiicod\bannersection\models\BannerImageModel',
                    'fieldName' => 'name',
                    'fieldBannerId' => 'bannerId',
                    'fieldAlt' => 'alt',
                    'fieldLink' => 'link',
                    'fieldText' => 'text',
                )
            ),
            'controllers' => array(
                'controllerMap' => array(
                    'admin' => array(
                        'banner' => 'yiicod\bannersection\controllers\admin\BannerController',
                        'bannerImage' => 'yiicod\bannersection\controllers\admin\BannerImageController',
                    )
                ),
                'admin' => array(
                    'banner' => array(
                        'layout' => '/layouts/column1',
                        'filters' => array('accessControl'),
                        'accessRules' => array(
                        ),
                    ),
                    'bannerImage' => array(
                        'layout' => '/layouts/column1',
                        'filters' => array('accessControl'),
                        'accessRules' => array(
                        ),
                    ),
                ),
            ),
            'components' => array(),
        ),
)

'preload' => array('bannersection')

Widget
------

$this->widget('yiicod\bannersection\widgets\BannerWidget', array(
    'position' => application\models\enumerables\BannerPosition::POSITION_TOP,
    'imageHtmlOptions' => array(
        'width' => 500,
        'height' => 100
    ))
);
        

```