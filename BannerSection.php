<?php

namespace yiicod\bannersection;

use CMap;
use Yii;
use CApplicationComponent;

/**
 * Cms extension settings
 * @author Orlov Alexey <aaorlov88@gmail.com>
 */
class BannerSection extends CApplicationComponent
{

    /**
     * @var ARRAY table settings
     */
    public $modelMap = [];

    /**
     * @var ARRAY components settings
     */
    public $components = [];

    /**
     * @var ARRAY position array('positionKey_INT' => 'positionName_STRING',)
     * In new versions deprecated
     */
    public $positions = [];

    /**
     * @var ARRAY coun banner per position
     * In new versions deprecated
     */
    public $countPerPosition = [];

    /**
     * @var ARRAY coun banner per position
     * In new versions deprecated
     */
    public $typePerPosition = [];

    /**
     * @var array Controllers settings
     */
    public $controllers = [];

    /**
     * Cache duration
     * @var int 
     */
    public $cacheDuration = 86400;

    public function init()
    {
        parent::init();
        //Merge main extension config with local extension config
        $config = include(dirname(__FILE__) . '/config/main.php');
        foreach ($config as $key => $value) {
            if (is_array($value)) {
                $this->{$key} = CMap::mergeArray($value, $this->{$key});
            } elseif (null === $this->{$key}) {
                $this->{$key} = $value;
            }
        }
        //For old begin
        $this->modelMap['Banner']['position'] = CMap::mergeArray($this->modelMap['Banner']['position'], $this->positions);
        $this->modelMap['Banner']['countPerPosition'] = CMap::mergeArray($this->modelMap['Banner']['countPerPosition'], $this->countPerPosition);
        //For old versions end

        if (!Yii::app() instanceof \CConsoleApplication) {
            //Merge controllers map
            $route = Yii::app()->urlManager->parseUrl(Yii::app()->request);
            $module = substr($route, 0, strpos($route, '/'));
            if (Yii::app()->hasModule($module) && isset($this->controllers['controllerMap'][$module])) {
                Yii::app()->getModule($module)->controllerMap = CMap::mergeArray($this->controllers['controllerMap'][$module], Yii::app()->getModule($module)->controllerMap);
            } elseif (isset($this->controllers['controllerMap']['default'])) {
                Yii::app()->controllerMap = CMap::mergeArray($this->controllers['controllerMap']['default'], Yii::app()->controllerMap);
            }
        }
        Yii::import($this->modelMap['Banner']['class']);
        Yii::import($this->modelMap['BannerImage']['class']);

        Yii::setPathOfAlias('yiicod', realpath(dirname(__FILE__) . '/..'));

        //Set components
        if (count($this->components)) {
            $exists = Yii::app()->getComponents(false);
            foreach ($this->components as $component => $params) {
                if (isset($exists[$component]) && is_object($exists[$component])) {
                    unset($this->components[$component]);
                } elseif (isset($exists[$component])) {
                    $this->components[$component] = \CMap::mergeArray($params, $exists[$component]);
                }
            }
            Yii::app()->setComponents(
                    $this->components, false
            );
        }
    }

}
