<?php

namespace yiicod\bannersection\helpers;
/**
 * \yiicod\bannersection\helpers\DataHelper::model('Banner', false)
 */
use CHtml;
use Yii;

class DataHelper extends CHtml
{

    public static function model($key, $string = true)
    {
        if ($string) {
            return Yii::app()->getComponent('bannersection')->modelMap[$key]['class'];
        } else {
            $model = Yii::app()->getComponent('bannersection')->modelMap[$key]['class'];
            return new $model;
        }
    }

}
